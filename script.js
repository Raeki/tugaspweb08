function validateForm() {
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    var errorMessage = document.getElementById("errorMessage");

    if (username === "" || password === "") {
        errorMessage.textContent = "Username and password must be filled out.";
        return false;
    }

    // Clear any previous error message
    errorMessage.textContent = "";

    // If additional validation is needed, add here

    alert("Login successful!");
    return true; // Return false to prevent form submission for demo purposes
}
